# Springboot3学习——Swagger3使用（四）

> ## 0、Swagger介绍
>
> ‌Swagger是一个用于描述、设计和文档化RESTful Web服务的规范。它允许以清晰、可理解的方式定义API，并提供工具来生成API文档、客户端SDK、服务器端存根等。Swagger3强调对RESTful API的支持和规范化，提供了更丰富和灵活的定义方式‌。我们主要使用其生成API文档，以及在界面测试API接口的功能

> ## 1、pom.xml添加springdoc-openapi-starter-webmvc-ui依赖
>
> ```xml
> <dependency>
>    	<groupId>org.springdoc</groupId>
>    	<artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
>    	<version>2.3.0</version>
>    </dependency>
>    ```
>    

> ## 2、运行成功，如下图
>
> ![image-20241017141813941](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/4.1.png)
> 
> 大功告成！

> *ps:题外话，在配置Swagger的过程中，走了不少弯路，分享给大家*
>
> *关于依赖，最终用了`springdoc-openapi-starter-webmvc-ui`，但是在网上刚开始查找的时候，还有其他依赖`springfox-swagger2`，`springfox-swagger-ui`这两个是一组的，还有`springfox-boot-starter`，还有`springdoc-openapi-ui`，但是最后都尝试失败了，页面显示资源不存在。*
>
> ![image-20241017142813032](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/4.2.png)
>
> *包括最后，添加依赖`springdoc-openapi-starter-webmvc-ui`，依然显示上诉错误，最后做了如下操作，终于显示成功了。清楚了无效缓存（猜测可能是之前引入了swagger2，操作了一堆导致了问题）*
>
> ![image-20241017142935896](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/4.3.png)

