# Springboot3学习——Cors处理跨域问题（六）

> ## 1、跨域CORS
>
> ‌*跨域（Cross-Origin Resource Sharing，简称‌CORS）是一种安全策略，用于限制一个域的网页如何与另一个域的资源进行交互‌。这是浏览器实施的‌同源策略（Same-Origin Policy）的一部分，旨在防止恶意网站通过一个域的网页访问另一个域的敏感数据。跨域的判断标准很简单，只要当一个请求URL的协议、域名、端口三者之间任意一个与当前页面URL不同即为跨域。最为常见的场景，就是当我们前后端分离的项目，前端调用后端接口，因端口号不同，就会产生一个跨域请求,当一个页面的URL为http://localhost:5173/login，通过‌axios向http://localhost:8080/show?userid=SV001发送请求，如下图*
>
> ![image-20241021155910549](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/6.1.png)

> ## 2、处理跨域问题（使用CorsFilter进行全局跨域配置）
>
> *在springboot项目config包下，创建CorsConfig*
>
> ```java
> package com.ziyan.config;
> 
> import org.springframework.context.annotation.Bean;
> import org.springframework.context.annotation.Configuration;
> import org.springframework.web.cors.CorsConfiguration;
> import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
> import org.springframework.web.filter.CorsFilter;
> 
> @Configuration
> public class CorsConfig {
> 
>     //当前跨域请求最大有效时长，默认1天
>     private static final long MAX_AGE = 24 * 60 * 60;
> 
>     @Bean
>     public CorsFilter corsFilter(){
>         CorsConfiguration corsConfiguration = new CorsConfiguration();
>         corsConfiguration.addAllowedOrigin("*");// 1 设置访问源地址
>         corsConfiguration.addAllowedHeader("*");// 2 设置访问源请求头
>         corsConfiguration.addAllowedMethod("*");// 3 设置访问源请求方法
>         corsConfiguration.setMaxAge(MAX_AGE);
>         UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
>         source.registerCorsConfiguration("/**", corsConfiguration);
>         return new CorsFilter(source);
>     }
> }
> 
> 
> ```
>
> 

> ## 3、运行
>
> ![image-20241021160143416](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/6.2.png)
>
> 测试成功！！！