# Springboot3学习——打包部署之使用WinSW（八）

> ## 1、前言
>
> 在项目过程中，开发完一个项目，我们需要部署到UAT环境，让用户测试，不可避免就需要学会打包，部署。
>

> ## 2、使用Maven Package打成Jar包
>
> ![image-20241024155935426](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/8.1.png)

> ## 3、使用java -jar命令
>
> 打开cmd控制台，使用java -jar命令
>
> ![image-20241024160319669](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/8.2.png)
> 
>    运行成功，访问[swagger](http://localhost:8080/swagger-ui/index.html)，测试
>    
>    ![image-20241024160348713](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/8.3.png)

> ## 4、使用WinSW
>
> cmd的控制台很容易一不小心就关闭，这里我们借助WinSW工具，将jar包部署成windows服务，用来方便管理程序的启动与运行。
>
> 1、下载WinSW,访问[WinSW地址](https://github.com/winsw/winsw/releases)，下载如下两个文件
>
> ![image-20241024160620246](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/8.4.png)
>
> 2、将下载的文件和jar包放在同一目录（可以任意重命名我们统一成zyp-service），修改xml文件
>
> ```xml
> <service>
>   
>   <!-- ID of the service. It should be unique across the Windows system-->
>   <id>zypservice</id>
>   <!-- Display name of the service -->
>   <name>zypservice</name>
>   <!-- Service description -->
>   <description>This service is a service created from a minimal configuration</description>
>   
>   <!-- Path to the executable, which should be started -->
>   <executable>java</executable>
>   <!--jar包的执行参数 -jar后面的是项目名-->
>   <arguments>-Dfile.encoding=utf-8 -Xmx256m -jar zyp-service.jar</arguments>
> 
> </service>
> ```
>
> ![image-20241024160817657](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/8.5.png)
>
> 3、运行`zyp-service.exe install` 安装服务，如下图
>
> ![image-20241024160927980](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/8.6.png)
>
> 访问[swagger](http://localhost:8080/swagger-ui/index.html)，部署成功
>
> 4、运行`zyp-service.exe uninstall`,卸载服务。