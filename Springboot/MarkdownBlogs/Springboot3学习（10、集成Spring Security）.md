# Springboot3学习——集成Spring Security（十）

> ## 1、前言
>
> ​	Spring Security是一个功能强大且高度可定制的身份验证和访问控制框架。它是保护基于Spring的应用程序的事实标准。

> ## 2、pom.xml添加依赖spring-boot-starter-security
>
> ```xml
>  <dependency>
>        <groupId>org.springframework.boot</groupId>
>        <artifactId>spring-boot-starter-security</artifactId>
>     </dependency>
> ```

> ## 3、Config包下，添加SecurityConfig
>
> ```java
> package com.ziyan.config;
> 
> 
> import org.springframework.context.annotation.Bean;
> import org.springframework.context.annotation.Configuration;
> import org.springframework.security.config.Customizer;
> import org.springframework.security.config.annotation.web.builders.HttpSecurity;
> import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
> import org.springframework.security.core.userdetails.User;
> import org.springframework.security.core.userdetails.UserDetails;
> import org.springframework.security.core.userdetails.UserDetailsService;
> import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
> import org.springframework.security.crypto.password.PasswordEncoder;
> import org.springframework.security.provisioning.InMemoryUserDetailsManager;
> import org.springframework.security.web.SecurityFilterChain;
> 
> @Configuration
> @EnableWebSecurity
> public class SecurityConfig {
> 
>     @Bean
>     public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
>         http
>                 .authorizeHttpRequests(auth -> auth
>                         .requestMatchers("/").permitAll()  // 公开访问
>                         .anyRequest().authenticated()
>                         // 其他接口需认证
>                 )
>                 .formLogin(Customizer.withDefaults())
>                 .httpBasic(Customizer.withDefaults())
>                 ;  // 使用 HTTP Basic 认证
>         return http.build();
>     }
> 
> 
>     @Bean
>     public UserDetailsService userDetailsService(PasswordEncoder passwordEncoder) {
>         // 创建用户
>         UserDetails user = User.builder()
>                 .username("SV001")
>                 .password(passwordEncoder.encode("123"))
>                 .roles("USER")
>                 .build();
>         return new InMemoryUserDetailsManager(user);
>     }
> 
>     @Bean
>     public PasswordEncoder passwordEncoder() {
> 
>         return new BCryptPasswordEncoder();  // 使用 BCrypt 进行密码加密
>     }
> }
> 
> ```
>
> 

> ## 4、运行
>
> 访问[swagger](http://localhost:8080/swagger-ui/index.html#/)，会跳到默认的login登录页面，输入账号密码，之后即可访问。如下图
>
> ![10.1](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/10.1.gif)
>
