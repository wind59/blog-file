# Springboot3学习——Mybatis-Generator使用及配置（三）

> ## 前言
>
> 上文我们在使用官方模版，成功运行的Springboot项目，接入了Mybatis，并且构造了web三层。详见[Springoot3学习——接入MyBatis（二）](https://blog.csdn.net/weixin_38696566/article/details/142981429?spm=1001.2014.3001.5501)其中大部分的代码主要在构造实体类，dao层接口上，手写非常的麻烦。

> ## 0、Mybatis-Generator介绍
>
> MyBatis Generator (MBG) 是一个 MyBatis 的代码生成器，它可以自动生成包括Mapper接口、Mapper XML文件以及实体类的代码。使用MBG可以极大地减少手动编写这些代码的时间，提高开发效率。

> ## 1、pom.xml添加mybatis-generator-maven-plugin插件
>
> ```xml
> <build>
>         <plugins>
>             <plugin>
>                 <groupId>org.springframework.boot</groupId>
>                 <artifactId>spring-boot-maven-plugin</artifactId>
>             </plugin>
>             <plugin>
>                 <groupId>org.mybatis.generator</groupId>
>                 <artifactId>mybatis-generator-maven-plugin</artifactId>
>                 <version>1.4.0</version>
>                 <dependencies>
>                     <!-- MyBatis Generator -->
>                     <dependency>
>                         <groupId>org.mybatis.generator</groupId>
>                         <artifactId>mybatis-generator-core</artifactId>
>                         <version>1.4.0</version>
>                     </dependency>
>                     <dependency>
>                         <groupId>mysql</groupId>
>                         <artifactId>mysql-connector-java</artifactId>
>                         <version>8.0.33</version>
>                     </dependency>
>                 </dependencies>
>                 <configuration>
>                     <!--允许移动生成的文件 -->
>                     <verbose>true</verbose>
>                     <!-- 是否覆盖 -->
>                     <overwrite>true</overwrite>
>                     <!-- 自动生成的配置 -->
>                     <configurationFile>
>                         ${project.basedir}/src/main/resources/mybatis-generator/mybatis-generator.xml
>                     </configurationFile>
>                 </configuration>
>             </plugin>
>         </plugins>
>     </build>
> ```
>
> (**ps:这里需要在org.mybatis.generator添加依赖mysql-connector-java，不然生成时会报如下错**：Failed to execute goal org.mybatis.generator:mybatis-generator-maven-plugin:1.4.0:generate (default-cli) on project zyp: ``Execution default-cli of goal org.mybatis.generator:mybatis-generator-maven-plugin:1.4.0:generate failed: Exception getting JDBC Driver``: com.mysql.cj.jdbc.Driver -> [Help 1])
>
> ![image-20241017083654547](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/3.1.png)

> ## 2、添加mybatis-generator.xml
>
> ```xml
> <?xml version="1.0" encoding="UTF-8"?>
> <!DOCTYPE generatorConfiguration
>         PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
>         "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">
> <generatorConfiguration>
>     <context id="MysqlContext" targetRuntime="MyBatis3">
>         <commentGenerator>
>             <property name="suppressDate" value="true"/>
>             <property name="suppressAllComments" value="true"/>
>         </commentGenerator>
>         <jdbcConnection driverClass="com.mysql.cj.jdbc.Driver"
>                         connectionURL="jdbc:mysql://localhost:3306/sugar"
>                         userId="root"
>                         password="lyh2016">
>         </jdbcConnection>
>         <javaTypeResolver>
>             <property name="forceBigDecimals" value="false"/>
>         </javaTypeResolver>
>         <javaModelGenerator targetPackage="com.ziyan.pojo" targetProject="src/main/java"/>
>         <sqlMapGenerator targetPackage="mapper" targetProject="src/main/resources"/>
> <!--        <javaClientGenerator targetPackage="com.ziyan.dao" targetProject="src/main/java" type="XMLMAPPER"/>-->
> <!--        -->
>         <javaClientGenerator targetPackage="com.ziyan.dao" targetProject="src/main/java" type="ANNOTATEDMAPPER"/>
> 
>         <table tableName="user" domainObjectName="User" enableCountByExample="false" enableUpdateByExample="false" enableDeleteByExample="false" enableSelectByExample="false" selectByExampleQueryId="false">
>             <!-- 配置属性映射 -->
>         </table>
> 
>     </context>
> </generatorConfiguration>
> ```
>
> 这里生成有两种形式：（项目中使用的是注解方式）
>
> * XML方式 `type="XMLMAPPER"`
>
> * 注解方式 `type="ANNOTATEDMAPPER"`
>
> 此外，这里我们配置了domainObjectName="User" enableCountByExample="false" enableUpdateByExample="false" enableDeleteByExample="false" enableSelectByExample="false" selectByExampleQueryId="false" 避免生成一堆example,看的眼花缭乱。

> ## 3、执行
>
> 执行成功，如下图
>
> ![image-20241017084605624](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/3.2.png)
>
> 因为我们配置的覆盖文件，可以发现之前的文件都被修改了，之前项目中的getUser方法，我们替换成mybatis-generator自动生成的CRUD方法中的selectByPrimaryKey。运行项目，如下图
>
> ![image-20241017085025106](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/3.3.png)
>
> ![image-20241017085037796](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/3.4.png)
>
> 运行成功！