# Springboot3学习——Druid使用及配置（五）

> ## 0、Druid
>
> *启动项目，我们可以看到Springboot3自带的数据库连接池是HikariPool，HikariPool的主要优点是高性能，而我们即将集成的Druid数据库连接池，主要有点则是丰富的扩展以及优秀的监控性能。从学习的角度来讲，Druid相比较其他数据库连接池而言，更加的全面。*
>
> ![image-20241018082511630](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/5.1.png)
>
> 

> ## 1、pom.xml添加druid-spring-boot-3-starter依赖
>
> Springboot3集成Druid，我们使用`druid-spring-boot-3-starter`包。springboot2则使用的是`druid-spring-boot-starter`包
>
> ```xml
> <dependency>
>     <groupId>com.alibaba</groupId>
>     <artifactId>druid-spring-boot-3-starter</artifactId>
>     <version>1.2.23</version>
> </dependency>
> ```
>

> ## 2、添加application.yml
>
> 我们新增application.yml，来配置druid相关配置，注释掉application.properties（ymlx相比于properties，更加的简洁和易读）,配置如下
>
> ```yml
> spring:
>   application:
>     name: zyp
>   datasource:
>     type: com.alibaba.druid.pool.DruidDataSource #设置数据源的类型
>     driver-class-name: com.mysql.cj.jdbc.Driver #指定MySQL的JDBC驱动类名
>     url: jdbc:mysql://localhost:3306/sugar #数据库连接的URL地址
>     username: root #数据库登录的用户名
>     password: lyh2016 #数据库登录的密码
>     druid:
>       initial-size: 5 #初始创建时建立的连接数
>       min-idle: 5 #最小空闲连接数
>       max-active: 20 #最大的连接池数量
>       max-wait: 60000 #获取连接的最大等待毫秒数
>       time-between-eviction-runs-millis: 60000 #两个连接空闲检查之间的时间间隔
>       min-evictable-idle-time-millis: 30000 #连接在池中最小生存时间后才允许被驱逐
>       validation-query: select 'x' #检查连接是否可用的 SQL 查询
>       test-while-idle: true #申请连接前检测连接是否有效
>       test-on-borrow: false #申请连接时进行连接有效性检验
>       test-on-return: false #归还连接时进行连接有效性检验
>       pool-prepared-statements: false #是否开启 PreparedStatement 的缓存
>       filters: stat,wall,slf4j #配置扩展插件，常用的有 stat（监控统计）、wall（SQL防火墙）、log4j（日志）等
>       max-pool-prepared-statement-per-connection-size: -1 #每个连接上PSCache最大语句数
>       use-global-data-source-stat: true #是否开启合并Stat
>       connect-properties: druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000 #连接属性，用于合并 SQL 和设置慢 SQL 的阈值
>       web-stat-filter:
>         enabled: true #是否启用该过滤器
>         url-pattern: /*
>         exclusions: /druid/*,*.js,*.gif,*.jpg,*.png,*.css,*.ico
>       stat-view-servlet:
>         enabled: true #是否启用监控页面
>         url-pattern: /druid/*
>         reset-enable: false #是否允许重置监控数据
>         login-username: druid #登录监控页面所需的用户名
>         login-password: druid #登录监控页面所需的密码
>         allow: 127.0.0.1 #允许访问监控页面的 IP 地址或地址段列表
>         deny: #明确拒绝访问监控页面的 IP 地址或地址段列表。如果留空，则默认拒绝所有未在 allow 中列出的 IP 地址。
> ```
>
> 

> ## 3、启动
>
> 成功运行，我们可以看到数据库连接池已经变成Druid。
>
> ![image-20241018084636216](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/5.2.png)
>
> 由于我们在配置文件中，启用了监控页面，我们来登录http://localhost:8080/druid/login.html![image-20241018084818542](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/5.3.png)
>
> 输入配置的账号密码，进入监控页面![image-20241018085019500](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/5.4.png)
>
> 我们点击SQL监控，并登录swagger地址http://localhost:8080/swagger-ui/index.html#/（如何配置使用swagger,详情见[Springboot3学习（4、Swagger使用及配置）](https://blog.csdn.net/weixin_38696566/article/details/143020857?spm=1001.2014.3001.5501)，调用show接口，查看效果,如下图。
>
> ![image-20241018085206432](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/5.5.png)
>
> 大功告成！！！

