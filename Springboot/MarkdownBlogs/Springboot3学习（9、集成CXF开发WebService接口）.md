# Springboot3学习——集成CXF开发WebService接口（九）

> ## 1、前言
>
> 在之前的项目过程中，我们已经开发过WebApi接口，但是在项目的实际应用场景中，我们还需要学会开发WebService接口，特别是在工厂以及企业应用集成中，使用较为广泛。
>
> > **WebService接口**
> > WebService通常指的是一个遵循SOAP（Simple Object Access Protocol）协议的服务，它定义了一种通过HTTP传输XML消息的标准。WebService通常包括以下组件：
> >
> > - SOAP：一种基于XML的消息协议，用于交换结构化信息。
> > - WSDL（Web Services Description Language）：一种基于XML的语言，用于描述服务、端点以及网络服务使用的绑定。
> > - UDDI（Universal Description, Discovery and Integration）：一种目录服务，企业可以注册自己的Web服务以便其他企业发现。
> >   WebService强调的是强类型和严格的格式化，适合于企业级应用集成，尤其是在需要跨平台互操作性和事务支持的环境中。

> ## 2、pom.xml添加依赖cxf-spring-boot-starter-jaxws
>
> ```xml
> <dependency>
>     <groupId>org.apache.cxf</groupId>
>     <artifactId>cxf-spring-boot-starter-jaxws</artifactId>
>     <version>4.0.5</version>
> </dependency>
> ```

> ## 3、新建webservice包，添加UserWebService
>
> ```java
> package com.ziyan.webservice;
> 
> import com.ziyan.pojo.User;
> import jakarta.jws.WebMethod;
> import jakarta.jws.WebService;
> 
> @WebService(name = "UserWebService", // 暴露服务名称
>         targetNamespace = "http://webservice.ziyan.com"// 命名空间,一般是接口的包名倒序
> )
> public interface UserWebService {
>     //根据ID获取用户信息
>     @WebMethod
>     User selectByPrimaryKey(String userid);
> }
> ```
>
> 

> ## 4、新建webservice.impl包，添加UserWebServiceImpl
>
> ```java
>package com.ziyan.webservice.impl;
> 
>import com.ziyan.dao.UserMapper;
> import com.ziyan.pojo.User;
>import com.ziyan.webservice.UserWebService;
> import jakarta.jws.WebService;
>import org.springframework.beans.factory.annotation.Autowired;
> import org.springframework.stereotype.Component;
> 
>   @WebService(serviceName = "UserWebService", // 与接口中指定的name一致
>           targetNamespace = "http://webservice.ziyan.com", // 与接口中的命名空间一致,一般是接口的包名倒
>           endpointInterface = "com.ziyan.webservice.UserWebService"// 接口地址
>   )
>   @Component
>   public class UserWebServiceImpl implements UserWebService {
>   
>       @Autowired
>       UserMapper userMapper;
>   
>       @Override
>       public User selectByPrimaryKey(String userid) {
>         return userMapper.selectByPrimaryKey(userid);
>     }
> }
>
> 
>```
> 
>

> ## 5、添加WebServiceConfig
>
> ```java
> package com.ziyan.config;
> 
> import com.ziyan.webservice.UserWebService;
> import jakarta.xml.ws.Endpoint;
> import org.apache.cxf.Bus;
> 
> import org.apache.cxf.jaxws.EndpointImpl;
> import org.springframework.beans.factory.annotation.Autowired;
> import org.springframework.context.annotation.Bean;
> import org.springframework.context.annotation.Configuration;
> 
> @Configuration
> public class WebServiceConfig {
>     @Autowired
>     private Bus bus;
>     @Autowired
>     private UserWebService userService;
> 
>     @Bean
>     public Endpoint endpointUserService() {
>         EndpointImpl endpoint = new EndpointImpl(bus,userService);
>         endpoint.publish("/UserWebService");//接口发布在 /UserService 目录下
>         return endpoint;
>     }
> }
> 
> ```
>
> 

> ## 6、运行
>
> zypapplication，添加"com.ziyan.webservice"扫描，运行，访问http://localhost:8080/services，显示下图
>
> ![image-20241030101130562](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/9.1.png)
>
> ![image-20241030101137073](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/9.2.png)
>
> 运行成功！

> ## 7、测试
>
> 运行soapUI测试，成功！
>
> ![image-20241030101711674](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/9.3.png)