# Springoot3学习——搭建SpringBoot（一）

> ## **1、生成模版**
>
> *可以在官网https://start.spring.io/生成模版，如下图*
>
> ![image-20241015151118652](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/1.1.png)
>
> 
>
> 

> ## **2、IDEA导入模板**
>
> *解压，打开IDEA，选择打开项目即可，如下图*
>
> ![image-20241015152120292](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/1.2.png)
>
> 

> ps：*找到application,运行，显示如下警告，运行失败。（os:这个报错是因为我们官网springboot模板创建选择的java17,而我之前本地环境是jdk1.8，本地切换下jdk17即可）*
>
> ![image-20241015153720435](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/1.3.png)
>
> *下载，安装JDK17，如下图*
>
> ![image-20241015154015008](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/1.4.png)
>
> *修改Project Structure*
>
> ![image-20241015161448635](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/1.5.png)
>
> *修改Settings*
>
> ![image-20241015161616522](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/1.6.png)
>
> 

> ## **3、运行成功**
>
> ![image-20241015161033232](https://gitee.com/wind59/blog-file/raw/master/Springboot/Pictures/1.7.png)
>
> 