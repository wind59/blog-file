package com.ziyan.webservice.impl;

import com.ziyan.dao.UserMapper;
import com.ziyan.pojo.User;
import com.ziyan.webservice.UserWebService;
import jakarta.jws.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@WebService(serviceName = "UserWebService", // 与接口中指定的name一致
        targetNamespace = "http://webservice.ziyan.com", // 与接口中的命名空间一致,一般是接口的包名倒
        endpointInterface = "com.ziyan.webservice.UserWebService"// 接口地址
)
@Component
public class UserWebServiceImpl implements UserWebService {

    @Autowired
    UserMapper userMapper;

    @Override
    public User selectByPrimaryKey(String userid) {
        return userMapper.selectByPrimaryKey(userid);
    }
}

