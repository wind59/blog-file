package com.ziyan.webservice;

import com.ziyan.pojo.User;
import jakarta.jws.WebMethod;
import jakarta.jws.WebService;

@WebService(name = "UserWebService", // 暴露服务名称
        targetNamespace = "http://webservice.ziyan.com"// 命名空间,一般是接口的包名倒序
)
public interface UserWebService {
    //根据ID获取用户信息
    @WebMethod
    User selectByPrimaryKey(String userid);
}