package com.ziyan.config;

import com.ziyan.webservice.UserWebService;
import jakarta.xml.ws.Endpoint;
import org.apache.cxf.Bus;

import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebServiceConfig {
    @Autowired
    private Bus bus;
    @Autowired
    private UserWebService userService;

    @Bean
    public Endpoint endpointUserService() {
        EndpointImpl endpoint = new EndpointImpl(bus,userService);
        endpoint.publish("/UserWebService");//接口发布在 /UserService 目录下
        return endpoint;
    }
}
