package com.ziyan.service.impl;

import com.ziyan.dao.UserMapper;
import com.ziyan.pojo.User;
import com.ziyan.service.UserService;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserMapper userMapper;

    @Override
    public User selectByPrimaryKey(String userid) {

        return userMapper.selectByPrimaryKey(userid);
    }

    @Override
    public int insert(User record) {

        return userMapper.insert(record);
    }

    @Override
    public User login(String userid,String pass) {

        return userMapper.login(userid, pass);
    }

    @Override
    public List<User> selectAll() {

        return userMapper.selectAll();
    }

    @Override
    public List<User> importUsers(InputStream inputStream) throws Exception {
        List<User> userList = new ArrayList<>();

        Workbook workbook = WorkbookFactory.create(inputStream);
        Sheet sheet = workbook.getSheetAt(0); // 假设用户信息在第一个 Sheet 中

        Iterator<Row> rowIterator = sheet.iterator();
        Date now=new Date();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (row.getRowNum() == 0) { // 跳过表头
                continue;
            }

            User user = new User();
            user.setUserid(new DataFormatter().formatCellValue(row.getCell(0)));
            user.setUsername(new DataFormatter().formatCellValue(row.getCell(1)));
            user.setDept(new DataFormatter().formatCellValue(row.getCell(2)));
            user.setPassword(new DataFormatter().formatCellValue(row.getCell(3)));
            user.setGender(new DataFormatter().formatCellValue(row.getCell(4)));
            user.setTransdatetime(now);
            user.setStatus("1");
            user.setCreateuid("sys");
            userList.add(user);
        }
        workbook.close();
        userList.forEach(user -> userMapper.insert(user));
        return userList;
    }


    @Override
    public void exportUsers(List<User> userList, HttpServletResponse response) throws Exception {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("用户信息");

        // 创建表头
        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("USERID");
        headerRow.createCell(1).setCellValue("USERNAME");
        headerRow.createCell(2).setCellValue("DEPT");
        headerRow.createCell(3).setCellValue("GENDER");
        // 添加更多字段...

        // 写入数据
        int rowNum = 1;
        for (User user : userList) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(user.getUserid());
            row.createCell(1).setCellValue(user.getUsername());
            row.createCell(2).setCellValue(user.getDept());
            row.createCell(3).setCellValue(user.getGender());
        }

        // 设置响应头
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        //response.setHeader("Content-Disposition","attachment;filename=用户表.xlsx");
        response.setHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode("用户表.xlsx","UTF-8"));
        // 输出到响应流
        workbook.write(response.getOutputStream());
        workbook.close();
    }
}