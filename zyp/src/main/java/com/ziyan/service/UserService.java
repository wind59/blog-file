package com.ziyan.service;

import com.ziyan.pojo.User;
import jakarta.servlet.http.HttpServletResponse;

import java.io.InputStream;
import java.util.List;

public interface UserService {
    User selectByPrimaryKey(String userid);
    int  insert(User record);
    User login(String userid,String pass);
    List<User> selectAll();
    List<User> importUsers(InputStream inputStream) throws Exception;
    void exportUsers(List<User> userList, HttpServletResponse response) throws Exception;
}
