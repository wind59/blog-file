package com.ziyan.dao;

import com.ziyan.pojo.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

public interface UserMapper {
    @Delete({
        "delete from user",
        "where userid = #{userid,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String userid);

    @Insert({
        "insert into user (userid, username, ",
        "dept, status, password, ",
        "gender, createuid, ",
        "transdatetime)",
        "values (#{userid,jdbcType=VARCHAR}, #{username,jdbcType=VARCHAR}, ",
        "#{dept,jdbcType=VARCHAR}, #{status,jdbcType=VARCHAR}, #{password,jdbcType=VARCHAR}, ",
        "#{gender,jdbcType=VARCHAR}, #{createuid,jdbcType=VARCHAR}, ",
        "#{transdatetime,jdbcType=TIMESTAMP})"
    })
    int insert(User record);

    @InsertProvider(type=UserSqlProvider.class, method="insertSelective")
    int insertSelective(User record);

    @Select({
        "select",
        "userid, username, dept, status, password, gender, createuid, transdatetime",
        "from user",
        "where userid = #{userid,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="userid", property="userid", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR),
        @Result(column="dept", property="dept", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.VARCHAR),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="gender", property="gender", jdbcType=JdbcType.VARCHAR),
        @Result(column="createuid", property="createuid", jdbcType=JdbcType.VARCHAR),
        @Result(column="transdatetime", property="transdatetime", jdbcType=JdbcType.TIMESTAMP)
    })
    User selectByPrimaryKey(String userid);

    @UpdateProvider(type=UserSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(User record);

    @Update({
        "update user",
        "set username = #{username,jdbcType=VARCHAR},",
          "dept = #{dept,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=VARCHAR},",
          "password = #{password,jdbcType=VARCHAR},",
          "gender = #{gender,jdbcType=VARCHAR},",
          "createuid = #{createuid,jdbcType=VARCHAR},",
          "transdatetime = #{transdatetime,jdbcType=TIMESTAMP}",
        "where userid = #{userid,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(User record);

    @Select({
            "select",
            "userid, username, dept, status, password, gender, createuid, transdatetime",
            "from user",
            "where userid = #{userid,jdbcType=VARCHAR} and password = #{pass,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="userid", property="userid", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR),
            @Result(column="dept", property="dept", jdbcType=JdbcType.VARCHAR),
            @Result(column="status", property="status", jdbcType=JdbcType.VARCHAR),
            @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
            @Result(column="gender", property="gender", jdbcType=JdbcType.VARCHAR),
            @Result(column="createuid", property="createuid", jdbcType=JdbcType.VARCHAR),
            @Result(column="transdatetime", property="transdatetime", jdbcType=JdbcType.TIMESTAMP)
    })
    User login(String userid,String pass);


    @Select({
            "select",
            "userid, username, dept, status, password, gender, createuid, transdatetime",
            "from user"
    })
    @Results({
            @Result(column="userid", property="userid", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR),
            @Result(column="dept", property="dept", jdbcType=JdbcType.VARCHAR),
            @Result(column="status", property="status", jdbcType=JdbcType.VARCHAR),
            @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
            @Result(column="gender", property="gender", jdbcType=JdbcType.VARCHAR),
            @Result(column="createuid", property="createuid", jdbcType=JdbcType.VARCHAR),
            @Result(column="transdatetime", property="transdatetime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<User> selectAll();
}