package com.ziyan.dao;

import com.ziyan.pojo.User;
import org.apache.ibatis.jdbc.SQL;

public class UserSqlProvider {
    public String insertSelective(User record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("user");
        
        if (record.getUserid() != null) {
            sql.VALUES("userid", "#{userid,jdbcType=VARCHAR}");
        }
        
        if (record.getUsername() != null) {
            sql.VALUES("username", "#{username,jdbcType=VARCHAR}");
        }
        
        if (record.getDept() != null) {
            sql.VALUES("dept", "#{dept,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            sql.VALUES("status", "#{status,jdbcType=VARCHAR}");
        }
        
        if (record.getPassword() != null) {
            sql.VALUES("password", "#{password,jdbcType=VARCHAR}");
        }
        
        if (record.getGender() != null) {
            sql.VALUES("gender", "#{gender,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateuid() != null) {
            sql.VALUES("createuid", "#{createuid,jdbcType=VARCHAR}");
        }
        
        if (record.getTransdatetime() != null) {
            sql.VALUES("transdatetime", "#{transdatetime,jdbcType=TIMESTAMP}");
        }
        
        return sql.toString();
    }

    public String updateByPrimaryKeySelective(User record) {
        SQL sql = new SQL();
        sql.UPDATE("user");
        
        if (record.getUsername() != null) {
            sql.SET("username = #{username,jdbcType=VARCHAR}");
        }
        
        if (record.getDept() != null) {
            sql.SET("dept = #{dept,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            sql.SET("status = #{status,jdbcType=VARCHAR}");
        }
        
        if (record.getPassword() != null) {
            sql.SET("password = #{password,jdbcType=VARCHAR}");
        }
        
        if (record.getGender() != null) {
            sql.SET("gender = #{gender,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateuid() != null) {
            sql.SET("createuid = #{createuid,jdbcType=VARCHAR}");
        }
        
        if (record.getTransdatetime() != null) {
            sql.SET("transdatetime = #{transdatetime,jdbcType=TIMESTAMP}");
        }
        
        sql.WHERE("userid = #{userid,jdbcType=VARCHAR}");
        
        return sql.toString();
    }
}