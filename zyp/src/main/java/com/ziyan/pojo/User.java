package com.ziyan.pojo;

import java.util.Date;

public class User {
    private String userid;

    private String username;

    private String dept;

    private String status;

    private String password;

    private String gender;

    private String createuid;

    private Date transdatetime;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCreateuid() {
        return createuid;
    }

    public void setCreateuid(String createuid) {
        this.createuid = createuid;
    }

    public Date getTransdatetime() {
        return transdatetime;
    }

    public void setTransdatetime(Date transdatetime) {
        this.transdatetime = transdatetime;
    }
}