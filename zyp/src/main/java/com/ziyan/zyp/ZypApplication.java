package com.ziyan.zyp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.ziyan.controller","com.ziyan.service","com.ziyan.webservice","com.ziyan.config"})
@MapperScan(basePackages = {"com.ziyan.dao"})

public class ZypApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZypApplication.class, args);
	}

}
