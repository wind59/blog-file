package com.ziyan.controller;

import com.ziyan.pojo.User;
import com.ziyan.service.UserService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    UserService userService;


    @PostMapping(value="/users", consumes = "multipart/form-data")
    public String importUsers(@RequestPart MultipartFile file) {
        try {
            List<User> userList = userService.importUsers(file.getInputStream());
            return "导入成功";
        } catch (Exception e) {
            e.printStackTrace();
            return "导入失败";
        }
    }
    @GetMapping("/users")
    public void exportUsers(HttpServletResponse response) {
        try {
            List<User> userList = userService.selectAll(); // 假设获取所有用户信息的方法
            userService.exportUsers(userList, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
