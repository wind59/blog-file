package com.ziyan.controller;

import com.ziyan.pojo.User;
import com.ziyan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class LoginController {
    @Autowired
    UserService userService;

    @GetMapping("/show")
    public User getUser(String userid){
        return userService.selectByPrimaryKey(userid);
    }


    @GetMapping("/insert")
    public int insertUser(String userid){
        User user=new User();
        user.setUserid(userid);
        user.setPassword("123");
        user.setStatus("1");
        user.setGender("女");
        user.setUsername("张三");
        user.setCreateuid("SV001");
        user.setDept("IT");
        user.setTransdatetime(new Date());
        return userService.insert(user);
    }


    @GetMapping("/login")
    public User getUser(String userid,String pass){
        return userService.login(userid,pass);
    }




}